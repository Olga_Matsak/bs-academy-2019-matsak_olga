const assert = require('assert');
const {URL} = require('url');
describe('webdriver.io page', () => {

    xit('should have the right title', () => {
        browser.url('https://webdriver.io');
        const title = browser.getTitle();
        assert.equal(title, 'WebdriverIO · Next-gen WebDriver test framework for Node.js');
    });

    xit ('should login to Hedonist', ()=>{

       browser.url('http://staging.bsa-hedonist.online');

       const emailField=$('input[name="email"]');
       const passField=$('input[type="password"]');
       const loginButton=$('button.button.is-primary');

       emailField.setValue('m.atsak.olga0110@gmail.com');
       passField.setValue('Tulenchik1616');
       loginButton.click();

       browser.pause(1000);

       const url = new URL(browser.getUrl());
       const actualUrl = url.hostname.toString() + url.pathname.toString();

    assert.equal(actualUrl,"staging.bsa-hedonist.online/search");
    });

    xit ('should register new user to Hedonist', ()=>{

        browser.url('staging.bsa-hedonist.online/signup');

        const firstName = $('input[name="firstName"]');
        const lastName = $('input[name="lastName"]');
        const email = $('input[name="email"]');
        const newPassword =$('input[type="password"]');
        const createButton = $('button.button.is-primary');

        firstName.setValue("Liolchik");
        lastName.setValue("Matsak");
        email.setValue('omatsak@rightandabove.com');
        newPassword.setValue('Rightandabove1');
        createButton.click();
 
        browser.pause(1000);

        const url = new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();

        assert.equal(actualUrl,"staging.bsa-hedonist.online/login");
    });

    xit ('login to Hedonist with invalid creds', ()=>{

        browser.url('http://staging.bsa-hedonist.online/login');

        const emailField=$('input[name="email"]');
        const passField=$('input[type="password"]');
        const loginButton=$('button.button.is-primary');
 
        emailField.setValue('m.atsak.olga0110gmail.com');
        passField.setValue('0');
        loginButton.click();
 
        browser.pause(1000);
 
        const url = new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
 
     assert.equal(actualUrl,"staging.bsa-hedonist.online/login");
     });

     xit ('add new place', ()=>{

        browser.url('staging.bsa-hedonist.online/places/add');

        const name= $('input.input.is-medium');
        const city= $('input .input[placeholder="Location"]');
        const zip= $('input[placeholder="09678"]');
        const address =$('input[placeholder="Khreschatyk St., 14"]');
        const phone = $('input[placeholder="+380961112233"]');
        const website = $('input[placeholder="http://the-best-place.com/"]');
        const description =$('textarea');
        const nextButton = $('.button.is-successful');
 
        name.setValue('Haveasnack');
        city.setValue('Kiev');
        address.setValue(4450);
        zip.setValue('Khreschatyk St., 10');
        phone.setValue(380961112233);
        website.setValue(+380961112233);
        description.setValue(' это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил');
        nextButton.click();
        const url = new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();

    assert.equal(actualUrl,"staging.bsa-hedonist.online/search");
    });
});
